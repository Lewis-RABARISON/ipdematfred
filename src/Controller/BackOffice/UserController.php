<?php

namespace App\Controller\BackOffice;

use App\Entity\User;
use App\Form\UserType;
use App\Form\ProfilType;
use App\Form\RoleUserType;
use App\Form\PswUpdateType;
use App\Entity\PasswordUpdate;
use App\Repository\UserRepository;
use App\Service\UploadFileService;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    
    public function __construct(UploadFileService $uploadFileService)
    {
        $this->uploadFileService = $uploadFileService;
    }

    /**
     * @Route("/compte-des-utilisateurs", name="user")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function index(UserRepository $userRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('BackOffice/user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ajouter-un-utilisateur", name="ajout_user")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function UserNouv(Request $request,EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            // $roles = $this->get('role')->getData();
            // dd($roles);
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $this->uploadFileService->uploadFile($form, $user, "picture", "upload_images_users_directory");
            $manager->persist($user);
            $manager->flush();
            
            $agent_id = $user->getId();
            $user->setNumCom("IP000".$agent_id);
            // $manager->persist($user);
            $manager->flush();
            
            $this->addFlash(
                'success',
                "L’<strong>utilisateur</strong> a été bien enregistrer"
            );
            return $this->redirectToRoute('user');
        }

        return $this->render('BackOffice/user/ajout.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/modification-de-role-d'agent", name="edit_role")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function EditUserRole(Request $request,User $user, EntityManagerInterface $manager)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(RoleUserType::class, $user);   
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
            // $roles = $this->get('role')->getData();
            // dd($roles);
            // $hash = $encoder->encodePassword($user, $user->getPassword());
            // $user->setPassword($hash);
            // $this->uploadFileService->uploadFile($form, $user, "picture", "upload_images_users_directory");
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('user');
        }

        return $this->render('BackOffice/user/role.html.twig',[
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/modification-de-profile", name="profil_edit")
     */
    public function EditeUserProfile(Request $request, EntityManagerInterface $manager)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();

        $form = $this->createForm(ProfilType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
            // $roles = $this->get('role')->getData();
            // dd($roles);
            // $hash = $encoder->encodePassword($user, $user->getPassword());
            // $user->setPassword($hash);
            // $this->uploadFileService->uploadFile($form, $user, "picture", "upload_images_users_directory");
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                "Les données du ont été modifier avec succès!"
            );
            
            return $this->redirectToRoute('profil_edit');
        }

        return $this->render('BackOffice/user/edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/modifier-mot-de-passe", name="password")
     */
    public function ModifierMDP(Request $request,UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();

        $passwordUpdate = new PasswordUpdate();
        $form = $this->createForm(PswUpdateType::class, $passwordUpdate);

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            if(!password_verify($passwordUpdate->getOldPassword(), $user->getPassword())){

                    $form->get('oldPassword')->addError(new FormError("Le mot de passe que vous avez tapé n'est pas votre mot de passe  actuel"));

            } else {
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $encoder->encodePassword($user, $newPassword);

                $user->setPassword($hash);
                
                $manager->persist($user);
                $manager->Flush();
 
                $this->addFlash(
                    'success',
                    "Votre mot de passe a bien été modifié!"
                );

                return $this->redirectToRoute('profil_edit');

            }    


        }

        return $this->render('BackOffice/user/pwd.html.twig', [
            'form'=>$form->createView()
        ]);
    }

// /**
//  * @Route("/mon-profile", name="profil")
//  */
//     public function Profil(UserRepository $userRepository):response
//     {
        
//         return $this->render("BackOffice/user/profil.html.twig",[
//             'users'=>$ $this->getUser()
//         ]);
//     }
}
