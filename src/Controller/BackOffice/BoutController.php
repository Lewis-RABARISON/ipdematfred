<?php

namespace App\Controller\BackOffice;

use App\Entity\Bout;
use App\Form\BoutType;
use App\Repository\BoutRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BoutController extends AbstractController
{
    /**
     * @Route("/boutiques", name="bout")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function index(BoutRepository $boutRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('BackOffice/bout/index.html.twig', [
            'bouts' => $boutRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ajouter-nouveau-boutique", name="bout_nouv")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function Boutique(Request $request,EntityManagerInterface $manager)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }


        $bout = new Bout();
        $form = $this->createForm(BoutType::class, $bout);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
            {
                $manager->persist($bout);
                $manager->flush();
                $this->addFlash(
                    'success',
                    "La boutique a été bien enregistrer"
                );
                return $this->redirectToRoute('bout');
            }

        return $this-> render('BackOffice/bout/bout.html.twig',[
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/modification-de-boutique", name="bout_edit")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function BoutEdit(Request $request,EntityManagerInterface $manager, Bout $bout)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(BoutType::class, $bout);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
            {
                $manager->persist($bout);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "La boutique a été bien modifié avec succès"
                );
                return $this->redirectToRoute('bout');
            }

        return $this-> render('BackOffice/bout/edit.html.twig',[
            'form'=> $form->createView()
        ]);
    }

       /**
     * @Route("/{id}/Supprimer-un-boutique", name="bout_suppr")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function delete(EntityManagerInterface $manager,Bout $bout)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $manager->remove($bout);
        $manager->flush();

        return $this->redirectToRoute('bout');
    }
}
