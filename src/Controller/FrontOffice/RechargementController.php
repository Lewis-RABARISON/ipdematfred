<?php

namespace App\Controller\FrontOffice;

use App\Entity\Soldes;
use App\Entity\Rechargement;
use App\Form\RechargementType;
use App\Repository\SoldesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RechargementController extends AbstractController
{
    /**
     * @Route("/rechargement", name="solde")
     * @IsGranted("ROLE_USER")
     * @IsGranted("ROLE_AGENT", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’agent!")
     */
    public function index(RechargementRepository $rechargementRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        return $this->render('FrontOffice/rechargement/index.html.twig', [
            'rechargements' => $rechargementRepository->findBy(['agent' => $user],['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/rechargemet-de-solde", name="recharge")
     * @IsGranted("ROLE_AGENT", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’agent!")
     */
    public function Solde(Request $request,EntityManagerInterface $manager,SoldesRepository $soldesRepository)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        $rechargement = new Rechargement();
        $rechargement->setAgent($user);
        $form = $this->createForm(RechargementType::class, $rechargement);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() )
            {
                $manager->persist($rechargement);

                $solde = new Soldes();
                $montant = $soldesRepository->findBy(['agent' => $user],['id' => 'DESC']);
                if(count($montant)){
                    $montant = $montant[0]->getMontant();
                } else{
                    $montant = 0;
                }

                $montant_recu = (int) $form->get("monRecu")->getData();
                $montant += $montant_recu; 
                $solde->setMontRecu($montant_recu);
                $solde->setAgent($user);
                $solde->setMontant($montant);
                $manager->persist($solde);

                $manager->flush();

                return $this->redirectToRoute('home');
            }

        return $this->render('FrontOffice/rechargement/solde.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
