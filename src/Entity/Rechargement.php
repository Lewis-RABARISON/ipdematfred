<?php

namespace App\Entity;

use App\Repository\RechargementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RechargementRepository::class)
 */
class Rechargement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     */
    private $monRecu;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $obesr;

    /**
     * @ORM\ManyToOne(targetEntity=Bout::class, inversedBy="rechargements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bout;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="rechargements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agent;

    // public $montant;
    // public $montantRecu;
    // public $montantRecu;


    
    public function __construct()
    {
        $this->date=new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getMonRecu(): ?float
    {
        return $this->monRecu;
    }

    public function setMonRecu(float $monRecu): self
    {
        $this->monRecu = $monRecu;

        return $this;
    }

    public function getObesr(): ?string
    {
        return $this->obesr;
    }

    public function setObesr(?string $obesr): self
    {
        $this->obesr = $obesr;

        return $this;
    }

    public function getBout(): ?Bout
    {
        return $this->bout;
    }

    public function setBout(?Bout $bout): self
    {
        $this->bout = $bout;

        return $this;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): self
    {
        $this->agent = $agent;

        return $this;
    }
}
