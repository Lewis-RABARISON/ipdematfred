<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping\PostLoad;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 * fields={"email"},
 * message= "L'email que vous avez indiqué est déjà utilisé !" )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $prenom;


    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="6", minMessage="Votre mot de passe doit faire minimum 6 caractères")
     *  @Assert\EqualTo(propertyPath="Confir_pwd", message="Vous n'avez pas tapé le même mot de passe")
     */
    private $Password;

     /**
     * @Assert\EqualTo(propertyPath="Password", message="Vous n'avez pas tapé le même mot de passe")
     */
    public $Confir_pwd;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity=Soldes::class, mappedBy="agent", orphanRemoval=true)
     */
    private $soldes;

    /**
     * @ORM\OneToMany(targetEntity=Rechargement::class, mappedBy="agent", orphanRemoval=true)
     */
    private $rechargements;

    /**
     * @ORM\OneToMany(targetEntity=Depense::class, mappedBy="agent", orphanRemoval=true)
     */
    private $depenses;

    /**
     * @ORM\OneToMany(targetEntity=Depot::class, mappedBy="agent", orphanRemoval=true)
     */
    private $depots;

    /**
     * @ORM\OneToMany(targetEntity=Course::class, mappedBy="agent", orphanRemoval=true)
     */
    private $courses;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $numCom;

    public function __construct()
    {
        $this->soldes = new ArrayCollection();
        $this->rechargements = new ArrayCollection();
        $this->depenses = new ArrayCollection();
        $this->depots = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    
    public function getPassword(): ?string
    {
        return $this->Password;
    }
    
    public function setPassword(string $Password): self
    {
        $this->Password = $Password;
        
        return $this;
    }
    
    public function getUsername(): ?string
    {
        return $this->email;
    }
    
    
    public function eraseCredentials()
    {
        
    }
    public function getSalt()
    {
        
    }
    
    public function setNumCom(?string $numCom): self
    {
        $this->numCom = $numCom;

        return $this;
    }
    
    public function getPicture(): ?string
    {
        return $this->picture;
    }
    
    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;
        
        return $this;
    }
    
    public function getRoles()
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }
    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;
        
        return $this;
    }

    /**
     * @return Collection|Soldes[]
     */
    public function getSoldes(): Collection
    {
        return $this->soldes;
    }

    public function addSolde(Soldes $solde): self
    {
        if (!$this->soldes->contains($solde)) {
            $this->soldes[] = $solde;
            $solde->setAgent($this);
        }

        return $this;
    }

    public function removeSolde(Soldes $solde): self
    {
        if ($this->soldes->removeElement($solde)) {
            // set the owning side to null (unless already changed)
            if ($solde->getAgent() === $this) {
                $solde->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rechargement[]
     */
    public function getRechargements(): Collection
    {
        return $this->rechargements;
    }

    public function addRechargement(Rechargement $rechargement): self
    {
        if (!$this->rechargements->contains($rechargement)) {
            $this->rechargements[] = $rechargement;
            $rechargement->setAgent($this);
        }

        return $this;
    }

    public function removeRechargement(Rechargement $rechargement): self
    {
        if ($this->rechargements->removeElement($rechargement)) {
            // set the owning side to null (unless already changed)
            if ($rechargement->getAgent() === $this) {
                $rechargement->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Depense[]
     */
    public function getDepenses(): Collection
    {
        return $this->depenses;
    }

    public function addDepense(Depense $depense): self
    {
        if (!$this->depenses->contains($depense)) {
            $this->depenses[] = $depense;
            $depense->setAgent($this);
        }

        return $this;
    }

    public function removeDepense(Depense $depense): self
    {
        if ($this->depenses->removeElement($depense)) {
            // set the owning side to null (unless already changed)
            if ($depense->getAgent() === $this) {
                $depense->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Depot[]
     */
    public function getDepots(): Collection
    {
        return $this->depots;
    }

    public function addDepot(Depot $depot): self
    {
        if (!$this->depots->contains($depot)) {
            $this->depots[] = $depot;
            $depot->setAgent($this);
        }

        return $this;
    }

    public function removeDepot(Depot $depot): self
    {
        if ($this->depots->removeElement($depot)) {
            // set the owning side to null (unless already changed)
            if ($depot->getAgent() === $this) {
                $depot->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setAgent($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            // set the owning side to null (unless already changed)
            if ($course->getAgent() === $this) {
                $course->setAgent(null);
            }
        }

        return $this;
    }

    public function getNumCom(): ?string
    {
        return $this->numCom;
    }
}
