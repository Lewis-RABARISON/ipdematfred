<?php

namespace App\Form;

use App\Entity\Bout;
use App\Entity\Rechargement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RechargementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('monRecu', TextType::class,[
                'label'=>'Montant'
            ])
            ->add('obesr', TextareaType::class,[
                'label' => 'Observation',
                'required' => false
            ])
            ->add('bout', EntityType::class,[
                'class' => Bout::class,
                'choice_label' => 'nom',
                'label'=>'Boutique'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rechargement::class,
        ]);
    }
}
